$(document).ready(function() {


	// Scroll Events
	$(window).scroll(function(){

		var wScroll = $(this).scrollTop();

		// Activate menu
		if (wScroll > 20) {
			$('#navigation').addClass('active');
			$('#main-nav').addClass('active');
		}
		else {
			$('#navigation').removeClass('active');
			$('#main-nav').removeClass('active');
		};
	});

});
